<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/getID3.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'getid3_description' => 'Ce plugin permet de récupérer les métadonnées de fichiers audio ou vidéo et de les stocker en base de donnée. Il récupère également les images contenues dans les fichiers audio.
_ Il peut également écrire certains tags dans les métadonnées de fichiers sonores.
_ Pour ce faire, il utilise la librairie [GetID3->http://getid3.sourceforge.net/] sous licence [GNU/GPL v2->http://www.getid3.org/source/license.txt] ',
	'getid3_slogan' => 'Lecture les métadonnées audio et vidéos de documents et écriture les tags audio'
);
