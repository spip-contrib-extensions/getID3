<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/getID3.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_appliquer_cover_defaut' => 'Appliquer la pochette par défaut à tous les documents sonores sans vignette',

	// E
	'erreur_formats_ecriture_impossible' => 'L’écriture de tags sur les formats suivants est impossible :',
	'erreur_logiciels_indisponibles' => 'Vous ne pouvez écrire les tags sur tous les formats possibles. Certains logiciels sont indisponibles.',
	'erreur_necessite' => 'nécessite @soft@',
	'explication_cover_defaut' => 'À la mise en ligne de fichiers sonores, une vignette par défaut (URL à mettre ci-dessous) est associée au fichier son. Si l’option de réécriture des tags à la modification du logo est activée, la pochette des tags id3 sera également mise à jour.',

	// F
	'formulaire_modifier_id3' => 'Modifier les métadonnées de :',

	// I
	'info_album' => 'Album :',
	'info_artist' => 'Artiste :',
	'info_audiosamplerate' => 'Sample rate :',
	'info_bitrate' => 'Bitrate :',
	'info_bitrate_mode' => 'Mode :',
	'info_bits' => 'Résolution (bits) :',
	'info_channel_mode' => 'Mode (channel) :',
	'info_channels' => 'Nombre de canaux :',
	'info_codec' => 'Codec :',
	'info_comment' => 'Commentaire :',
	'info_comments' => 'Commentaires :',
	'info_commercial_information' => 'Informations commerciales :',
	'info_copyright' => 'Copyright :',
	'info_copyright_message' => 'Message de copyright :',
	'info_duree' => 'Durée :',
	'info_duree_secondes' => 'Durée (en secondes) :',
	'info_encoded_by' => 'Encodé par :',
	'info_encodeur' => 'Encodé par :',
	'info_encoding_time' => 'Date d’encodage :',
	'info_erreurs' => 'Erreurs',
	'info_extension' => 'Extension :',
	'info_format' => 'Format :',
	'info_gauche_numero_document' => 'Document numéro',
	'info_genre' => 'Genre :',
	'info_lossless' => 'Aucune perte de compression',
	'info_media' => 'Type de média :',
	'info_mime' => 'Type mime :',
	'info_nom_fichier' => 'Nom du fichier :',
	'info_original_filename' => 'Nom original',
	'info_original_release_time' => 'Date de création originale :',
	'info_sample_rate' => 'Sample rate :',
	'info_source' => 'Source :',
	'info_title' => 'Titre :',
	'info_totaltracks' => 'Nombre total de pistes :',
	'info_track' => 'Piste :',
	'info_track_number' => 'Piste numéro :',
	'info_url_artist' => 'Url de l’artiste :',
	'info_url_file' => 'Url du fichier :',
	'info_url_payment' => 'Url de paiement :',
	'info_url_publisher' => 'Url du site de publication :',
	'info_url_source' => 'Url de la source :',
	'info_url_station' => 'Url de station (?) :',
	'info_utilisation_aucune' => 'Aucune utilisation de ce document',
	'info_utilisation_plusieurs' => '@nb@ utilisations',
	'info_utilisation_unique' => 'Une utilisation',
	'info_year' => 'Année',

	// L
	'label_album' => 'Album',
	'label_artist' => 'Artiste',
	'label_comment' => 'Commentaire',
	'label_cover' => 'Pochette',
	'label_cover_defaut' => 'Utiliser une pochette par défaut',
	'label_genre' => 'Genre',
	'label_reecriture_tags' => 'Réécrire les tags des fichiers à la modification',
	'label_reecriture_tags_descriptif' => 'de la description du document',
	'label_reecriture_tags_logo' => 'du logo du document',
	'label_reecriture_tags_titre' => 'du titre du document',
	'label_title' => 'Titre',
	'label_verifier_logiciels' => 'Revérifier les logiciels',
	'label_year' => 'Année',
	'legende_ecriture_tags' => 'Écriture des tags',
	'lien_modifier_id3' => 'Modifier les tags audio',
	'lien_recuperer_infos' => 'Récupérer les informations du fichier',

	// M
	'message_cover_defaut_modifiee' => 'Un document a été modifié',
	'message_cover_defaut_modifiees' => '@nb@ documents ont été modifiés',
	'message_erreur_document_distant_ecriture' => 'Ce document est « distant » et ne peut donc pas être modifié.',
	'message_extension_invalide_ecriture' => 'Le format de ce fichier n’est pas pris en charge.',
	'message_fichier_maj' => 'Le fichier a été mis à jour.',
	'message_infos_document_distant' => 'Ce document est distant. Aucune information ne peut en être récupérée.',
	'message_texte_binaire_manquant' => 'Un logiciel nécessaire n’est pas disponible sur votre serveur :',
	'message_texte_binaires_informer' => 'Veuillez en informer votre administrateur.',
	'message_texte_binaires_manquant' => 'Plusieurs logiciels nécessaires ne sont pas disponibles sur votre serveur :',
	'message_titre_binaire_manquant' => 'Un logiciel manquant',
	'message_titre_binaires_manquant' => 'Plusieurs logiciels manquant',
	'message_validation_appliquer_cover' => 'Cette action est définitive. Il n’est pas possible de revenir en arrière par la suite.',
	'message_valider_cover_defaut' => 'Validez le formulaire pour associer la pochette par défaut',

	// S
	'son_bitrate_cbr' => 'Bitrate constant',
	'son_bitrate_vbr' => 'Bitrate variable',

	// T
	'titre_getid3' => 'GetID3',
	'titre_infos_techniques' => 'Informations techniques'
);
