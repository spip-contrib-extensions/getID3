<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-getid3?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'getid3_description' => 'Este plugin permite recuperar los metadatos de archivos audio o vídeo y almacenarlos en la base de datos. Recupera asimismo las imágenes contenidas en los archivos audio.
_ Puede también escribir algunas etiquetas en los metadatos de archivos sonoros.
_ Para ello, utiliza la biblioteca [GetID3->http://getid3.sourceforge.net/] bajo licencia [GNU/GPL v2->http://www.getid3.org/source/license.txt] ',
	'getid3_slogan' => 'Lectura de los metadatos audio y vídeos de documentos y escritura de las etiquetas audio'
);
