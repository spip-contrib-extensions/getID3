<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/getid3?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_appliquer_cover_defaut' => 'Predvolený obal použiť na všetky súbory so zvukmi bez miniatúry',

	// E
	'erreur_formats_ecriture_impossible' => 'Zápis značiek do týchto formátov sa nedá vykonať:',
	'erreur_logiciels_indisponibles' => 'Momentálne nemôžete zapisovať značky do všetkých dostupných formátov.  Niektoré programy nie sú totiž k dispozícii.',
	'erreur_necessite' => 'vyžaduje si @soft@',
	'explication_cover_defaut' => 'Pri spúšťaní súborov so zvukmi je predvolená miniatúra priradená k súboru so zvukom (URL sa dá nastaviť nižšie). Ak je aktivovaná možnosť prepisovania značiek úpravy log, obal so značkami id3 bude aktualizovaný tiež.',

	// F
	'formulaire_modifier_id3' => 'Upraviť metadata súboru:',

	// I
	'info_album' => 'Album:',
	'info_artist' => 'Umelec:',
	'info_audiosamplerate' => 'Rýchlosť ukážky:',
	'info_bitrate' => 'Bitová rýchlosť:',
	'info_bitrate_mode' => 'Režim:',
	'info_bits' => 'Rozlíšenie (v bitoch):',
	'info_channel_mode' => 'Režim (kanál):',
	'info_channels' => 'Počet kanálov:',
	'info_codec' => 'Kodek:',
	'info_comment' => 'Komentár:',
	'info_comments' => 'Komentáre:',
	'info_commercial_information' => 'Obchodné informácie:',
	'info_copyright' => 'Autorské práva:',
	'info_copyright_message' => 'Informácia o autorských právach:',
	'info_duree' => 'Dĺžka:',
	'info_duree_secondes' => 'Dĺžka (v sekundách):',
	'info_encoded_by' => 'Zakódoval:',
	'info_encodeur' => 'Zakódoval:',
	'info_encoding_time' => 'Dátum zakódovania:',
	'info_erreurs' => 'Chyby',
	'info_extension' => 'Prípona:',
	'info_format' => 'Formát:',
	'info_gauche_numero_document' => 'Súbor číslo',
	'info_genre' => 'Žáner:',
	'info_lossless' => 'Žiadna strata kompresie',
	'info_media' => 'Typ média:',
	'info_mime' => 'Typ mime:',
	'info_nom_fichier' => 'Názov súboru:',
	'info_original_filename' => 'Pôvodný názov',
	'info_original_release_time' => 'Dátum vytvorenia originálu:',
	'info_sample_rate' => 'Rýchlosť ukážky:',
	'info_source' => 'Zdroj:',
	'info_title' => 'Názov:',
	'info_totaltracks' => 'Celkový počet stôp:',
	'info_track' => 'Stopa:',
	'info_track_number' => 'Číslo stopy:',
	'info_url_artist' => 'Url umelca:',
	'info_url_file' => 'Url súboru:',
	'info_url_payment' => 'Url pre platbu:',
	'info_url_publisher' => 'Url stránky publikovania:',
	'info_url_source' => 'Url zdroja:',
	'info_url_station' => 'Url stanice (?) :',
	'info_utilisation_aucune' => 'Žiadne použitie tohto dokumentu',
	'info_utilisation_plusieurs' => '@nb@ použití',
	'info_utilisation_unique' => 'Jedno použitie',
	'info_year' => 'Rok',

	// L
	'label_album' => 'Album',
	'label_artist' => 'Umelec',
	'label_comment' => 'Komentár',
	'label_cover' => 'Obal',
	'label_cover_defaut' => 'Použiť predvolený obal',
	'label_genre' => 'Žáner',
	'label_reecriture_tags' => 'Prepísať značky súborov pri zmene',
	'label_reecriture_tags_descriptif' => 'z opisu súboru',
	'label_reecriture_tags_logo' => 'z loga súboru',
	'label_reecriture_tags_titre' => 'z názvu súboru',
	'label_title' => 'Názov',
	'label_verifier_logiciels' => 'Znova skontrolovať softvér',
	'label_year' => 'Rok',
	'legende_ecriture_tags' => 'Zápis značiek',
	'lien_modifier_id3' => 'Upraviť audio značky',
	'lien_recuperer_infos' => 'Zistiť informácie o súbore',

	// M
	'message_cover_defaut_modifiee' => 'Súbor bol upravený',
	'message_cover_defaut_modifiees' => 'bolo upravených @nb@ súborov',
	'message_erreur_document_distant_ecriture' => 'Tento dokument je "vzdialený", a preto sa nedá upravovať.',
	'message_extension_invalide_ecriture' => 'Formát tohto súboru nie je podporovaný.',
	'message_fichier_maj' => 'Súbor bol aktualizovaný.',
	'message_infos_document_distant' => 'Tento dokument je vzdialený. Nedajú sa o ňom získať žiadne informácie.',
	'message_texte_binaire_manquant' => 'Na vašom serveri nie je dostupný jeden potrebný program:',
	'message_texte_binaires_informer' => 'Prosím, kontaktujte svojho administrátora.',
	'message_texte_binaires_manquant' => 'Na vašom serveri nie je dostupných niekoľko potrebných programov:',
	'message_titre_binaire_manquant' => 'Chýba softvér',
	'message_titre_binaires_manquant' => 'Ďalší softvér, ktorý chýba',
	'message_validation_appliquer_cover' => 'Táto akcia je definitívna. Nedá sa neskôr odvolať.',
	'message_valider_cover_defaut' => 'Ak chcete nastaviť predvolený obal, potvrďte formulár',

	// S
	'son_bitrate_cbr' => 'Konštanta bitovej rýchlosti',
	'son_bitrate_vbr' => 'Premenná bitovej rýchlosti',

	// T
	'titre_getid3' => 'GetID3',
	'titre_infos_techniques' => 'Technické údaje'
);
