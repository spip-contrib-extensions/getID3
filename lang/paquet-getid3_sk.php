<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-getid3?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'getid3_description' => 'Tento zásuvný modul vám umožňuje získavať metadáta z audio a video súborov a ukladať ich do databázy. Získava aj obrázky v audio súboroch.
_Dokáže tiež zapisovať niektoré tagy metadát do audio súborov.
_Na to využíva knižnicu [GetID3->http://getid3.sourceforge.net/] s licenciou [GNU/GPL v2->http://www.getid3.org/source/license.txt] ',
	'getid3_slogan' => 'Čítanie metadát zvukových súborov a súborov videí a zápis audio tagov.'
);
